package com.mdw360.com.mdw360.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * TODO: add filename here example HelloWorld.java
 * Created on 14-10-27
 * Copyright(c) 2014 Middleware360 Solutions Inc. All Rights Reserved.
 * This software is the proprietary information of Middleware360 Solutions Inc.
 * Purpose:
 */
public class MyProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        System.out.println("MY OUTPUT: " + exchange.getIn().getBody(String.class));

    }
}
