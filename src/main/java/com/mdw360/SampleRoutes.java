package com.mdw360;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.mdw360.com.mdw360.processor.MyProcessor;

public class SampleRoutes extends RouteBuilder {
    private static final transient Logger LOG = LoggerFactory.getLogger(SampleRoutes.class);

    @Value("${echo.value}") String echoStringValue;

    @Override
    public void configure() throws Exception {
        from("timer://foo?fixedRate=true&period=10000")
                .to("direct:audit");

        from("direct:audit")
                .to("http://localhost:8080/metrics").convertBodyTo(String.class)
                .to("direct:logme", "direct:filedrop");

        from("direct:logme")
                .to("log:com.mdw360.SampleRoutes");

        from("direct:filedrop").process(new MyProcessor()).to("file:target/my_other_files");

    }

    public String getEchoStringValue() {
        return echoStringValue;
    }
}
